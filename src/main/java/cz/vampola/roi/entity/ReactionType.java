package cz.vampola.roi.entity;

public enum ReactionType {
    LOVE, HAHA, LIKE, WOW, SAD, ANGRY
}
