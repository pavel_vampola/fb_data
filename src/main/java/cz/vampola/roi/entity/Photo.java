package cz.vampola.roi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "photo")
public class Photo {
    @Id
    @Column(name = "photo_id")
    private Long id;
    @Column(name = "album_name")
    private String albumName;
    @Column(name = "fb_url")
    private String fbURL;
    @Column(name = "image_url")
    private String imageURL;
    @Column(name="user_id")
    private Long userId;

    @Column(name = "love_count")
    private int loveCount;
    @Column(name = "haha_count")
    private int hahaCount;
    @Column(name = "like_count")
    private int likeCount;
    @Column(name = "wow_count")
    private int wowCount;
    @Column(name = "sad_count")
    private int sadCount;
    @Column(name = "angry_count")
    private int angryCount;
}