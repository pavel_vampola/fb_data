package cz.vampola.roi.repositories;

import cz.vampola.roi.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
    List<Photo> findByUserId(Long userId);
//    @Query("SELECT p FROM Photo p  WHERE p.userId= (:userId)")
//    List<Photo> findByUserId(@Param("userId") Integer userId);
}