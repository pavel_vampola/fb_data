package cz.vampola.roi.services.facebook;

import cz.vampola.roi.entity.ReactionType;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Photo;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.vampola.roi.entity.ReactionType.*;

public class FacebookConnector {

    private long fbUserId;
    private Facebook facebook;

    public FacebookConnector(long fbUserId, String token) {
        this.fbUserId = fbUserId;
        facebook = new FacebookTemplate(token);
    }

    public cz.vampola.roi.entity.User getUser() {

        String[] fields = {"id", "name", "gender", "picture"};
        User fetchedUser = facebook.fetchObject(Long.toString(fbUserId), User.class, fields);
        cz.vampola.roi.entity.User entityUser = new cz.vampola.roi.entity.User();
        entityUser.setId(Long.valueOf(fetchedUser.getId()));
        entityUser.setName(fetchedUser.getName());
        entityUser.setGender(fetchedUser.getGender());

        Map picture = (Map) fetchedUser.getExtraData().get("picture");
        Map pictureData = (Map) picture.get("data");
        String pictureDataURL = (String) pictureData.get("url");
        entityUser.setPictureURL(pictureDataURL);
        return entityUser;
    }

    public List<cz.vampola.roi.entity.Photo> getPhotos() {
        String[] fields2 = {"album",
                "link",
                "picture",
                "gender",
                getReactionField(LIKE),
                getReactionField(LOVE),
                getReactionField(HAHA),
                getReactionField(WOW),
                getReactionField(SAD),
                getReactionField(ANGRY)};

        PagedList<Photo> photos = facebook.fetchConnections(Long.toString(fbUserId), "photos", Photo.class, fields2);

        List<cz.vampola.roi.entity.Photo> list = new ArrayList<>();
        photos.forEach(photo -> {
            cz.vampola.roi.entity.Photo entityPhoto = new cz.vampola.roi.entity.Photo();
            entityPhoto.setId(Long.valueOf(photo.getId()));
            if (photo.getAlbum() != null)
                entityPhoto.setAlbumName(photo.getAlbum().getName());
            entityPhoto.setFbURL(photo.getLink());
            entityPhoto.setImageURL(photo.getPicture());
            entityPhoto.setLikeCount(getReactionCountFromExtra(photo.getExtraData(), LIKE));
            entityPhoto.setHahaCount(getReactionCountFromExtra(photo.getExtraData(), HAHA));
            entityPhoto.setLoveCount(getReactionCountFromExtra(photo.getExtraData(), LOVE));
            entityPhoto.setWowCount(getReactionCountFromExtra(photo.getExtraData(), WOW));
            entityPhoto.setAngryCount(getReactionCountFromExtra(photo.getExtraData(), ANGRY));
            entityPhoto.setSadCount(getReactionCountFromExtra(photo.getExtraData(), SAD));
            entityPhoto.setUserId(fbUserId);
            list.add(entityPhoto);
        });
        return list;
    }

    private Integer getReactionCountFromExtra(Map<String, Object> extraData, ReactionType angry) {
        Map type = (Map) extraData.get(angry.toString());
        Map typeSummary = (Map) type.get("summary");
        return (Integer) typeSummary.get("total_count");
    }


    private String getReactionField(ReactionType reactionType) {
        return "reactions.type(" + reactionType + ").limit(0).summary(1).as(" + reactionType + ")";
    }
}
