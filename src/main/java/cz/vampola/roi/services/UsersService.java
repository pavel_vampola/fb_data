package cz.vampola.roi.services;

import cz.vampola.roi.entity.Photo;
import cz.vampola.roi.entity.User;
import cz.vampola.roi.repositories.PhotoRepository;
import cz.vampola.roi.repositories.UserRepository;
import cz.vampola.roi.services.facebook.FacebookConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class UsersService {

    private UserRepository userRepository;
    private PhotoRepository photoRepository;

    @Autowired
    public UsersService(UserRepository userRepository, PhotoRepository photoRepository) {
        this.userRepository = userRepository;
        this.photoRepository = photoRepository;
    }

    public void loadUserAndHisData(long fbUserId, String userToken) {
        FacebookConnector facebookConnector = new FacebookConnector(fbUserId, userToken);
        User user = facebookConnector.getUser();
        userRepository.save(user);
        List<Photo> photos = facebookConnector.getPhotos();
        photoRepository.saveAll(photos);
    }

    public User getDetail(long fbUserId) {
        return userRepository.findById(fbUserId).orElseThrow(EntityNotFoundException::new);
    }

    public List<Photo> getPhotos(long fbUserId) {
        return photoRepository.findByUserId(fbUserId);
    }

    public void delete(long fbUserId) {
        try {
            userRepository.deleteById(fbUserId);
        } catch (EmptyResultDataAccessException exc) {
            throw new EntityNotFoundException();
        }
    }
}
