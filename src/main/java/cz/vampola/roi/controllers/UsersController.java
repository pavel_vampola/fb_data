package cz.vampola.roi.controllers;

import cz.vampola.roi.entity.Photo;
import cz.vampola.roi.entity.User;
import cz.vampola.roi.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    private PhotoDTO map(Photo photo) {
        PhotoDTO photoDTO = new PhotoDTO();
        photoDTO.id = photo.getId();
        photoDTO.albumName = photo.getAlbumName();
        photoDTO.fbURL = photo.getFbURL();
        photoDTO.imageURL = photo.getImageURL();
        photoDTO.reactions = new ReactionsDTO();
        photoDTO.reactions.angry = photo.getAngryCount();
        photoDTO.reactions.haha = photo.getHahaCount();
        photoDTO.reactions.like = photo.getLikeCount();
        photoDTO.reactions.love = photo.getLoveCount();
        photoDTO.reactions.sad = photo.getSadCount();
        photoDTO.reactions.wow = photo.getWowCount();
        return photoDTO;
    }

    private List<PhotoDTO> map(List<Photo> photos) {
        return photos.stream().map(this::map).collect(Collectors.toList());
    }

    public static class FbUserAccessPayload {
        @Min(1)
        public Long id;
        @NotBlank
        public String token;

    }

    @PostMapping()
    public ResponseEntity loadUsersData(@RequestBody @Valid FbUserAccessPayload fbUserAccessPayload) {
        usersService.loadUserAndHisData(fbUserAccessPayload.id, fbUserAccessPayload.token);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @GetMapping("/{fbUserId}")
    public ResponseEntity getUsersDetail(@PathVariable long fbUserId) {
        User user = usersService.getDetail(fbUserId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/{fbUserId}/photos")
    public ResponseEntity getUsersPhotos(@PathVariable long fbUserId) {
        List<Photo> photos = usersService.getPhotos(fbUserId);
        return new ResponseEntity<>(map(photos), HttpStatus.OK);
    }

    @DeleteMapping("/{fbUserId}")
    public ResponseEntity deleteUser(@PathVariable long fbUserId) {
        usersService.delete(fbUserId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
