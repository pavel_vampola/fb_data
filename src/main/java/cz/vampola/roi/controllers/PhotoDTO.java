package cz.vampola.roi.controllers;

public class PhotoDTO {
    public Long id;
    public String albumName;
    public String fbURL;
    public String imageURL;
    public ReactionsDTO reactions;

}