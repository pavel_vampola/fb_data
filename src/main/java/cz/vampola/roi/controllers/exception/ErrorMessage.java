package cz.vampola.roi.controllers.exception;

import lombok.Data;

@Data
public class ErrorMessage {
    private String error;
}
