package cz.vampola.roi.controllers.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class HomeworkExceptionHandler {
    @ExceptionHandler({EntityNotFoundException.class, ResourceNotFoundException.class})
    public final ResponseEntity<ErrorMessage> handleEntityNotFound(Exception ex, WebRequest request) {
        ErrorMessage errorObj = new ErrorMessage();
        errorObj.setError(ex.getMessage());
        return new ResponseEntity<ErrorMessage>(errorObj, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidAuthorizationException.class)
    public final ResponseEntity<ErrorMessage> handleInvalidAuth(Exception ex, WebRequest request) {
        ErrorMessage errorObj = new ErrorMessage();
        errorObj.setError(ex.getMessage());
        return new ResponseEntity<ErrorMessage>(errorObj, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorMessage> handleAllExceptions(Exception ex, WebRequest request) {
        ErrorMessage errorObj = new ErrorMessage();
        errorObj.setError(ex.getMessage());
        return new ResponseEntity<ErrorMessage>(errorObj, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
