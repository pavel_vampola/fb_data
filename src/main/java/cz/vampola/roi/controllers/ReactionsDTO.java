package cz.vampola.roi.controllers;

public class ReactionsDTO {
    public int love;
    public int haha;
    public int like;
    public int wow;
    public int sad;
    public int angry;
}
