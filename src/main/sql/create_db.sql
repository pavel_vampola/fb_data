--CREATE DATABASE roi WITH owner=postgres template=template0 encoding='UTF8';

DROP TABLE IF EXISTS fbuser, photo CASCADE;

CREATE TABLE fbuser (
  user_id     bigint  PRIMARY KEY,
  name        TEXT    NOT NULL,
  gender      TEXT,
  picture_url TEXT
);

CREATE TABLE photo (
  photo_id   bigint PRIMARY KEY,
  album_name TEXT,
  gender     TEXT,
  fb_url     TEXT    NOT NULL,
  image_url  TEXT    NOT NULL,
  user_id    bigint  NOT NULL,
  like_count     INTEGER NOT NULL DEFAULT 0,
  haha_count     INTEGER NOT NULL DEFAULT 0,
  love_count     INTEGER NOT NULL DEFAULT 0,
  wow_count      INTEGER NOT NULL DEFAULT 0,
  sad_count      INTEGER NOT NULL DEFAULT 0,
  angry_count    INTEGER NOT NULL DEFAULT 0
);

ALTER TABLE photo
  ADD CONSTRAINT photo_user_fkey FOREIGN KEY (user_id) REFERENCES fbuser (user_id) ON UPDATE CASCADE ON DELETE CASCADE;

