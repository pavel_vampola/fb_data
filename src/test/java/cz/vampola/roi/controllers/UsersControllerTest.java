package cz.vampola.roi.controllers;

import cz.vampola.roi.entity.Photo;
import cz.vampola.roi.entity.User;
import cz.vampola.roi.services.UsersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static net.javacrumbs.jsonunit.spring.JsonUnitResultMatchers.json;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UsersController.class)
public class UsersControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UsersService usersService;

    @Test
    public void postLoadData_shouldReturnCreated() throws Exception {
        mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"id\":1,\"token\":\"userToken\"}"))
                .andExpect(status().isAccepted())
                .andExpect(
                        json().isEqualTo("")
                );

        verify(usersService, times(1)).loadUserAndHisData(1L, "userToken");
        verifyNoMoreInteractions(usersService);
    }

    @Test
    public void postLoadDataWithEmptyToken_shouldReturn4xx() throws Exception {
        mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"id\":\"userId\",\"token\":\"\"}"))
                .andExpect(status().is4xxClientError());

        verifyNoMoreInteractions(usersService);
    }

    private User user() {
        User user = new User();
        user.setId(1L);
        user.setGender("male");
        user.setName("John");
        user.setPictureURL("http://img.img");
        return user;
    }

    @Test
    public void getUser_shouldReturnUser() throws Exception {

        when(usersService.getDetail(1)).thenReturn(user());
        mockMvc.perform(get("/users/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(
                        json().isEqualTo("{\n" +
                                "  \"id\": 1,\n" +
                                "  \"name\": \"John\",\n" +
                                "  \"gender\": \"male\",\n" +
                                "  \"pictureURL\": \"http://img.img\"\n" +
                                "}")
                );

        verify(usersService, times(1)).getDetail(1);
        verifyNoMoreInteractions(usersService);
    }

    @Test
    public void getPhotos_shouldReturnUsersPhotos() throws Exception {

        when(usersService.getPhotos(1)).thenReturn(photos());
        mockMvc.perform(get("/users/1/photos")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(
                        json().isEqualTo("[\n" +
                                "  {\n" +
                                "    \"id\": 100,\n" +
                                "    \"albumName\": \"album\",\n" +
                                "    \"fbURL\": \"http://fb.img\",\n" +
                                "    \"imageURL\": \"http://img.img\",\n" +
                                "    \"reactions\": {\n" +
                                "      \"love\": 4,\n" +
                                "      \"haha\": 2,\n" +
                                "      \"like\": 3,\n" +
                                "      \"wow\": 5,\n" +
                                "      \"sad\": 6,\n" +
                                "      \"angry\": 1\n" +
                                "    }\n" +
                                "  },\n" +
                                "  {\n" +
                                "    \"id\": 101,\n" +
                                "    \"albumName\": \"album2\",\n" +
                                "    \"fbURL\": \"http://fb2.img\",\n" +
                                "    \"imageURL\": \"http://img2.img\",\n" +
                                "    \"reactions\": {\n" +
                                "      \"love\": 40,\n" +
                                "      \"haha\": 20,\n" +
                                "      \"like\": 30,\n" +
                                "      \"wow\": 50,\n" +
                                "      \"sad\": 60,\n" +
                                "      \"angry\": 10\n" +
                                "    }\n" +
                                "  }\n" +
                                "]")
                );

        verify(usersService, times(1)).getPhotos(1);
        verifyNoMoreInteractions(usersService);
    }

    private List<Photo> photos() {
        List<Photo> list = new ArrayList<>();
        Photo p1 = Photo.builder()
                .id(100L)
                .albumName("album")
                .fbURL("http://fb.img")
                .imageURL("http://img.img")
                .angryCount(1)
                .hahaCount(2)
                .likeCount(3)
                .loveCount(4)
                .wowCount(5)
                .sadCount(6)
                .build();
        Photo p2 = Photo.builder()
                .id(101L)
                .albumName("album2")
                .fbURL("http://fb2.img")
                .imageURL("http://img2.img")
                .angryCount(10)
                .hahaCount(20)
                .likeCount(30)
                .loveCount(40)
                .wowCount(50)
                .sadCount(60)
                .build();
        list.add(p1);
        list.add(p2);
        return list;
    }

    @Test
    public void deleteUser_shouldReturn204() throws Exception {

        mockMvc.perform(delete("/users/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent())
                .andExpect(
                        json().isEqualTo("")
                );

        verify(usersService, times(1)).delete(1);
        verifyNoMoreInteractions(usersService);
    }
}