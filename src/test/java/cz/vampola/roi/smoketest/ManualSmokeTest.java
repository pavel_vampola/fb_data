package cz.vampola.roi.smoketest;

import cz.vampola.roi.repositories.PhotoRepository;
import cz.vampola.roi.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static net.javacrumbs.jsonunit.spring.JsonUnitResultMatchers.json;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//CAUTION: It will delete DB
//@Ignore("Smoke is dependant on manualy added fb token")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ManualSmokeTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private
    UserRepository userRepository;

    @Autowired
    private
    PhotoRepository photoRepository;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void smokeTest() throws Exception {
        userRepository.deleteAll();
        Long userId = 10218867843793483L;
        String fbToken = "TOKEN_PLACEHOLDER";
        mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"id\": " + userId + ",\n" +
                        "  \"token\": \"" + fbToken + "\"\n" +
                        "}"))
                .andExpect(status().isAccepted());
        Assert.assertNotNull(userRepository.getOne(userId));
        Assert.assertTrue(photoRepository.findByUserId(userId).size() > 0);


        mockMvc.perform(get("/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

    }
}