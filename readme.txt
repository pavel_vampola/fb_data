Aplikace je napsaná spíše jako startovací/ demo aplikace. Rozšiřování by určitě vedlo k dalším úpravám a vylepšením jako např.:
* Používat verzování API
* Zaměřit se na to jak se používají objekty (DTO, business, entity). Tyto objekty se často od sebe začnou odlišovat nebo se o ně začnou starat jiní vývojáři, je dobré je oddělit. V menších appkách jako je tato je podle mě lepší to moc neoddělovat.
* Zlepšit komunikaci s FB API. Použil jsem spring-social-facebook, který mi nepřišel úplně dobrý, jednak API co nabízel bylo nedostatené a taky dokumentace byla velice stručná. Díky chybějící API jsem neimplementoval stránkování, takže bych přístě zvolil jinou knihovnu. Zároveň by bylo dobré to pokrýt testy.
* V DB začít používat role. Časem by se možná změnilo ORM, protože teď mapuju cizí klíč, místo celé entity.
* Nepoužívat spring.jpa.open-in-view=true.
* Zaměřit se na bezpečnost. Např. zabalit posílaná data do obálky
* Napsat integrační testy
* Implementovat restfull(HATEOAS)